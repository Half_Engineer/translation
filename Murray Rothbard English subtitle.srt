1
00:00:22,720 --> 00:00:28,520
In a sense, the entire system of taxation
is a form of involuntary servitude.

2
00:00:29,240 --> 00:00:31,520
Take in particular the income tax.

3
00:00:32,200 --> 00:00:38,700
The high levels of income tax mean that all of
us work a large part of the year, several months.

4
00:00:38,960 --> 00:00:43,740
For Uncle Sam, before being allowed to
enjoy our incomes on the market.

5
00:00:44,480 --> 00:00:50,839
Part of the essence of slavery after all is
for first work for someone at little or no pay.

6
00:00:51,233 --> 00:00:56,779
But the income tax means that we sweat and earn income only to see
the government

7
00:00:56,861 --> 00:01:00,748
 extract a large chunk of it by coercion for its own purposes.

8
00:01:01,460 --> 00:01:04,740
What is this, but forced labor at no pay?

9
00:01:05,700 --> 00:01:11,740
The withholding feature of the income tax is a
still more clear cut instance of involuntary servitude.

10
00:01:12,200 --> 00:01:17,639
For as the Intrepid Connecticut
Industrialist Vivian Kellums argued years ago,

11
00:01:17,664 --> 00:01:27,433
 the employer is forced to expand
time, labor and money in the business of deducting and transmitting his employees taxes to the federal and state governments,

12
00:01:27,458 --> 00:01:31,657
 yet the employer is not
recompanced for this expenditure.

13
00:01:32,300 --> 00:01:38,620
What moral principle justifies the government's
forcing employers to act as its unpaid tax collectors?

14
00:01:39,320 --> 00:01:44,941
The withholding principle of course is the
linchpin of the whole federal income tax system.

15
00:01:45,020 --> 00:01:50,894
Without the steady and relatively painless process of deducting the tax from the workers paycheck,

16
00:01:51,148 --> 00:01:56,866
the government could never hope to raise the high levels of tax from the workers in one lump sum.

17
00:01:57,645 --> 00:02:05,766
Few people remember that the withholding system was only
instituted during World War II and was supposed to be a wartime expedient.

18
00:02:06,220 --> 00:02:14,900
Like so many other features of state despotism, however, the wartime
emergency measure soon became a hallowed part of the American system.

19
00:02:16,370 --> 00:02:27,900
To add insult to injury, the individual taxpayer in filling out his tax form is also
forced by the government to work at no pay on the laborious and thankless task

20
00:02:28,000 --> 00:02:30,680
of reckoning how much he owes the
government.

21
00:02:31,580 --> 00:02:38,080
Here again, he cannot charge the government for
the cost and labor expended in making out his return.

22
00:02:38,453 --> 00:02:46,606
Furthermore, the law requiring everyone to fill out his tax
form is a clear violation of the Fifth Amendment of the Constitution

23
00:02:46,754 --> 00:02:50,768
 prohibiting the government from forcing anyone to incriminate himself.

24
00:02:51,640 --> 00:02:59,060
Yet the courts, often zealous in protecting Fifth Amendment
rights in less sensitive areas, have done nothing here.

25
00:02:59,231 --> 00:03:04,779
In a case where the entire existence of the swollen federal government structure is at stake

26
00:03:05,566 --> 00:03:15,672
the repeal of either the income tax or the withholding or self -incriminating provisions would force the
government back to the relatively minor levels of power that

27
00:03:15,697 --> 00:03:18,471
the country enjoyed before the 20th century.

